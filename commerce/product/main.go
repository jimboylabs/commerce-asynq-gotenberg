//go:generate happy
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// An Error that implements http.Handler to write structured JSON errors.
type Error struct {
	code    int
	message string
}

func Errorf(code int, format string, args ...interface{}) error {
	return Error{code, fmt.Sprintf(format, args...)}
}

func (e Error) Error() string { return e.message }

func (e Error) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(e.code)
	json.NewEncoder(w).Encode(map[string]string{"error": e.message})
}

type Product struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Service struct {
	products []Product
}

//happy:api GET /products
func (s *Service) ListProducts() ([]Product, error) {
	return s.products, nil
}

//happy:api GET /products/:id
func (s *Service) GetProduct(id string) (Product, error) {
	for _, product := range s.products {
		if product.ID == id {
			return product, nil
		}
	}

	return Product{}, Errorf(http.StatusNotFound, "product with id %s not found", id)
}

func main() {
	service := &Service{
		products: []Product{{ID: "product-001", Name: "SER Ryzen 7 4800u"}, {ID: "product-002", Name: "Intel NUC 12 PRO"}},
	}
	log.Fatal(http.ListenAndServe(":9000", service))
}
