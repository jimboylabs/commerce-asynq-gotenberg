# Commerce

Proof of concept using [asynq](https://github.com/hibiken/asynq) for background task scheduler and [gotenberg](https://gotenberg.dev/) for generate PDF.

## Installation

You will need to have the following installed or configured, before proceeding

- Docker and Docker compose
- K3d
- kubectl
- Go
- k6

### Before you start

Running local kubernetes cluster using k3d, first we need to create the cluster

```
k3d cluster create --config mycluster.yaml
```

Make sure the cluster is already running

```
k3d cluster list
```

Run tilt.dev

```
tilt up
```

Tilt ui should be running in `http://localhost:10350`

## Demo

https://youtu.be/9CRnTLzPIXc

### Screenshots

<details>
<summary>Tilt</summary>

![Tilt](screenshots/screenshot-asynq-2.png)

</details>

<details>
<summary>Asynqmon</summary>

![Tilt](screenshots/screenshot-asynq-1.png)

</details>

## Future Improvements

- Create internal handler for Invoice HTML rendering
- Explore gotenberg async mode (need to provide a callback once the job is finished)
