package product

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/wayanjimmy/commerce/pkg/rest"
)

type RESTService struct {
	BaseURL string
	Source  string
}

func (s *RESTService) FindProductByID(ctx context.Context, id string) (Product, error) {
	request := rest.RequestWithContext(ctx, s.Source)
	url := fmt.Sprintf("%s/products/%s", s.BaseURL, id)

	res, err := request.
		SetHeader("Content-Type", "application/json").
		Get(url)
	if err != nil {
		return Product{}, err
	}

	var product Product
	if err := json.Unmarshal(res.Body(), &product); err != nil {
		return Product{}, err
	}

	return product, nil
}
