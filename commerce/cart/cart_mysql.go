package cart

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/rs/xid"
	"gitlab.com/wayanjimmy/commerce/database"
)

var _ CartRepo = (*CartRepoMySQL)(nil)

type CartRepoMySQL struct {
	DB *database.Module
}

func (c *CartRepoMySQL) create(ctx context.Context) (string, error) {
	guid := xid.New()
	cartID := guid.String()

	data := struct {
		CartID  string `db:"cart_id"`
		Content string `db:"content"`
	}{
		CartID:  cartID,
		Content: "[]",
	}

	query := `INSERT INTO carts (cart_id, content) VALUES (:cart_id, :content)`

	query, args, err := sqlx.Named(query, data)
	if err != nil {
		return "", err
	}

	_, err = c.DB.ExecContext(ctx, query, args...)
	if err != nil {
		return "", err
	}

	return cartID, nil
}

func (c *CartRepoMySQL) FindByID(ctx context.Context, id string) (Cart, error) {
	query := `
	SELECT cart_id
		 , IFNULL(email, '')     as email
		 , IFNULL(content, '[]') as content
	FROM carts
	WHERE cart_id = ?`

	row := struct {
		CartID  string `db:"cart_id"`
		Email   string `db:"email"`
		Content string `db:"content"`
	}{}

	err := c.DB.GetContext(ctx, &row, query, id)
	if errors.Is(err, sql.ErrNoRows) {
		cartID, err := c.create(ctx)
		if err != nil {
			return Cart{}, err
		}

		return Cart{ID: cartID}, nil
	} else if err != nil {
		return Cart{}, err
	}

	cart := Cart{ID: row.CartID}

	var items []Item
	err = json.Unmarshal([]byte(row.Content), &items)
	if err != nil {
		return Cart{}, err
	}
	if len(items) > 0 {
		cart.Items = items
	}

	return cart, nil
}

func (c *CartRepoMySQL) Update(ctx context.Context, id string, items []Item) error {
	marshal, err := json.Marshal(items)
	if err != nil {
		return err
	}

	data := struct {
		Content string `db:"content"`
		CartID  string `db:"cart_id"`
	}{
		Content: string(marshal),
		CartID:  id,
	}

	query := `UPDATE carts SET content = :content WHERE cart_id = :cart_id`

	_, err = c.DB.NamedExecContext(ctx, query, data)
	if err != nil {
		return err
	}

	return nil
}
