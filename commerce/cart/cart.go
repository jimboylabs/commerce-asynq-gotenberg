package cart

import "errors"

type Item struct {
	ProductID string `json:"product_id"`
	Quantity  int    `json:"quantity"`
}

type Cart struct {
	ID    string `json:"id"`
	Items []Item `json:"items"`
}

func (c Cart) ValidateForCheckout() error {
	if len(c.Items) == 0 {
		return errors.New("items is empty")
	}

	return nil
}

type CheckoutRequest struct {
	PaymentMethod string `json:"payment_method"`
	Address       string `json:"address"`
}

type CheckoutResponse struct {
	OrderNumber string `json:"order_number"`
}
