trigger_mode(TRIGGER_MODE_MANUAL)
load('ext://dotenv', 'dotenv')
load('ext://restart_process', 'docker_build_with_restart')

dotenv()

docker_compose('./docker-compose.yaml')

dc_resource('mysql', labels=['database'], auto_init=False)
dc_resource('redis', labels=['database'], auto_init=False)

# cart service

local_resource(
	'be-cart-comp',
	'CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app',
	dir='./commerce/cart/main',
	deps=['**/*.go'],
	labels=['cart'],
	auto_init=False,
)

docker_build(
	'be-cart',
	context='./commerce/cart/main',
	dockerfile='./commerce/cart/main/Dockerfile',
	only=['./app']
)

k8s_yaml('commerce/cart/main/k8s.yaml')

k8s_resource(
	'be-cart',
	labels=['cart'],
	resource_deps=['be-cart-comp', 'redis'],
	auto_init=False,
)

local_resource(
	'be-cart-worker-comp',
	'CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app',
	dir='./commerce/cart/worker',
	deps=['**/*.go'],
	labels=['cart'],
	auto_init=False,
)

docker_build(
	'be-cart-worker',
	context='./commerce/cart/worker',
	dockerfile='./commerce/cart/worker/Dockerfile',
	only=['./app']
)

k8s_yaml('commerce/cart/worker/k8s.yaml')

k8s_resource(
	'be-cart-worker',
	labels=['cart'],
	resource_deps=['be-cart-worker-comp', 'redis'],
	auto_init=False,
)

# product service

local_resource(
	'be-product-comp',
	'CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app',
	dir='./commerce/product',
	deps=['**/*.go'],
	labels=['product'],
	auto_init=False,
)

docker_build(
	'be-product',
	context='./commerce/product',
	dockerfile='./commerce/product/Dockerfile',
	only=['./app'],
)

k8s_yaml('commerce/product/k8s.yaml')

k8s_resource(
	'be-product',
	labels=['product'],
	resource_deps=['be-product-comp'],
	auto_init=False,
)

# gotenberg

k8s_yaml('gotenberg/k8s.yaml')

k8s_resource(
	'gotenberg',
	auto_init=False,
	port_forwards=['9090:3000']
)

# asynqmon

k8s_yaml('asynqmon/k8s.yaml')

k8s_resource(
	'asynqmon',
	auto_init=False,
	port_forwards=['7070:8080'],
	resource_deps=['redis'],
)

#k6

local_resource(
	'k6',
	dir='./k6',
	cmd='k6 run --vus 50 --duration 5s script.js',
	auto_init=False,
	resource_deps=['be-cart', 'be-product', 'gotenberg', 'asynqmon', 'mysql', 'redis'],
)