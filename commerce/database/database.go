package database

import (
	"context"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Module struct {
	*sqlx.DB
	driver string
	dsn    string
}

func New(driver, dsn string) (*Module, error) {
	db, err := sqlx.Open(driver, dsn)
	if err != nil {
		return nil, err
	}

	m := &Module{
		db,
		driver,
		dsn,
	}

	return m, m.init()
}

func (m *Module) init() (err error) {
	switch m.driver {
	case "mysql":
		_, err = m.ExecContext(context.Background(), mysqlStartup)
	}

	return nil
}
