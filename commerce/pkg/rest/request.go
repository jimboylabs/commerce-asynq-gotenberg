package rest

import (
	"context"
	"github.com/go-resty/resty/v2"
)

func RequestWithContext(ctx context.Context, source string) *resty.Request {
	httpReq := getClient(source).R()
	httpReq.SetContext(ctx)

	return httpReq
}
