package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/hibiken/asynq"
	"gitlab.com/wayanjimmy/commerce/cart"
	"gitlab.com/wayanjimmy/commerce/cart/tasks"
	"gitlab.com/wayanjimmy/commerce/database"
	"gitlab.com/wayanjimmy/commerce/pkg/gotenberg"
	"gitlab.com/wayanjimmy/commerce/pkg/product"
)

func main() {
	host := "host.k3d.internal"

	redisAddr := fmt.Sprintf("%s:6379", host)

	srv := asynq.NewServer(
		asynq.RedisClientOpt{Addr: redisAddr},
		asynq.Config{
			Concurrency: 6,
			Queues: map[string]int{
				"critical": 6,
				"default":  3,
				"low":      1,
			},
		},
	)

	// open db connection
	dbModule, err := database.New("mysql", "root:root@tcp(host.k3d.internal:3306)/commerce?parseTime=true")
	if err != nil {
		log.Fatalln("unable to connect to database", err)
	}

	cartRepo := &cart.CartRepoMySQL{DB: dbModule}

	productClient := product.RESTService{
		BaseURL: "http://be-product.default.svc.cluster.local",
		Source:  "cart",
	}

	gotenbergClient := gotenberg.RESTService{
		BaseURL: "http://gotenberg.default.svc.cluster.local",
		Source:  "cart",
	}

	generateInvoiceHandler := &tasks.GenerateInvoiceHandler{
		CartRepo:  cartRepo,
		Product:   productClient,
		Gotenberg: gotenbergClient,
	}

	loggingMiddleware := func(h asynq.Handler) asynq.Handler {
		return asynq.HandlerFunc(func(ctx context.Context, t *asynq.Task) error {
			start := time.Now()
			log.Printf("Start processing %q", t.Type())
			err := h.ProcessTask(ctx, t)
			if err != nil {
				log.Println(err)
				return err
			}
			log.Printf("Finished processing %q: Elapsed Time = %v", t.Type(), time.Since(start))
			return nil
		})
	}
	mux := asynq.NewServeMux()
	mux.Use(loggingMiddleware)
	mux.Handle(tasks.TypeGenerateInvoice, generateInvoiceHandler)

	if err := srv.Run(mux); err != nil {
		log.Fatalf("could not run server: %v", err)
	}
}
