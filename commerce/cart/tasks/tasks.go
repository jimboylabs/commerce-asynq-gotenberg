package tasks

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/hibiken/asynq"
	"gitlab.com/wayanjimmy/commerce/cart"
	"gitlab.com/wayanjimmy/commerce/pkg/gotenberg"
	"gitlab.com/wayanjimmy/commerce/pkg/product"
)

const (
	TypeGenerateInvoice = "invoice:generate"
)

type GenerateInvoicePayload struct {
	OrderID string
}

func NewGenerateInvoiceTask(orderID string) (*asynq.Task, error) {
	payload, err := json.Marshal(GenerateInvoicePayload{OrderID: orderID})
	if err != nil {
		return nil, err
	}

	return asynq.NewTask(TypeGenerateInvoice, payload), nil
}

var _ asynq.Handler = (*GenerateInvoiceHandler)(nil)

type GenerateInvoiceHandler struct {
	CartRepo  cart.CartRepo
	Gotenberg gotenberg.RESTService
	Product   product.RESTService
	// put database here
}

func (h *GenerateInvoiceHandler) ProcessTask(ctx context.Context, t *asynq.Task) error {
	var p GenerateInvoicePayload
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		return fmt.Errorf("json.Unmarshal failed: %v: %w", err, asynq.SkipRetry)
	}

	// enrich data process
	// fetch data from database
	cart, err := h.CartRepo.FindByID(ctx, p.OrderID)
	if err != nil {
		return err
	}

	for _, item := range cart.Items {
		log.Printf("fetch product id: %s quantity: %d\n", item.ProductID, item.Quantity)
	}

	// call gotenberg service to generate the pdf
	// TODO: use static sample pdf view for now
	pdfBytes, err := h.Gotenberg.GeneratePDF(ctx, "https://sparksuite.github.io/simple-html-invoice-template")
	if err != nil {
		return err
	}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}

	dirPath := homeDir + "/invoices"
	_, err = os.Stat(dirPath)
	if os.IsNotExist(err) {
		err = os.Mkdir(dirPath, 0755)
		if err != nil {
			return err
		}
	}

	invoiceFilename := fmt.Sprintf("%s.pdf", p.OrderID)
	invoicePath := filepath.Join(dirPath, invoiceFilename)

	err = ioutil.WriteFile(invoicePath, pdfBytes, 0644)
	if err != nil {
		return err
	}

	return nil
}
