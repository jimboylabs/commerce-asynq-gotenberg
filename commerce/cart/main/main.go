//go:generate happy
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/hibiken/asynq"
	"gitlab.com/wayanjimmy/commerce/cart"
	"gitlab.com/wayanjimmy/commerce/cart/tasks"
	"gitlab.com/wayanjimmy/commerce/database"
	"gitlab.com/wayanjimmy/commerce/pkg/gotenberg"
	"gitlab.com/wayanjimmy/commerce/pkg/product"
)

// An Error that implements http.Handler to write structured JSON errors.
type Error struct {
	code    int
	message string
}

func Errorf(code int, format string, args ...interface{}) error {
	return Error{code, fmt.Sprintf(format, args...)}
}

func (e Error) Error() string { return e.message }

func (e Error) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(e.code)
	json.NewEncoder(w).Encode(map[string]string{"error": e.message})
}

type Service struct {
	asynqClient *asynq.Client
	cartRepo    cart.CartRepo
	product     product.RESTService
	gotenberg   gotenberg.RESTService
}

//happy:api GET /cart/:id
func (s *Service) GetCart(id string) (cart.Cart, error) {
	if id == "" {
		return cart.Cart{}, Errorf(http.StatusNotFound, "id can't be empty")
	}

	c, err := s.cartRepo.FindByID(context.Background(), id)
	if err != nil {
		return cart.Cart{}, Errorf(http.StatusNotFound, "cart %q not found", id)
	}

	if len(c.Items) == 0 {
		c.Items = make([]cart.Item, 0)
	}

	return c, nil
}

//happy:api POST /cart/:id/add
func (s *Service) CreateItem(id string, item cart.Item) error {
	ctx := context.Background()

	if item.ProductID == "" {
		return Errorf(http.StatusBadRequest, "product id can't be empty")
	}

	_, err := s.product.FindProductByID(ctx, item.ProductID)
	if err != nil {
		return Errorf(http.StatusNotFound, "product id %s not found", item.ProductID)
	}

	log.Printf("product added: id=%s cart_id=%s", item.ProductID, id)

	err = s.cartRepo.Update(ctx, id, []cart.Item{item})
	if err != nil {
		return Errorf(http.StatusInternalServerError, "something went wrong")
	}

	return nil
}

//happy:api POST /cart/:id/checkout
func (s *Service) Checkout(id string, request cart.CheckoutRequest) (cart.CheckoutResponse, error) {
	task, err := tasks.NewGenerateInvoiceTask(id)
	if err != nil {
		message := fmt.Sprintf("could not create generate invoice task: %s", id)
		log.Print(message)
		return cart.CheckoutResponse{}, Errorf(http.StatusInternalServerError, message)
	}

	info, err := s.asynqClient.Enqueue(task)
	if err != nil {
		message := fmt.Sprintf("could not enqueue generate invoice task: %s", id)
		log.Print(message)
		return cart.CheckoutResponse{}, Errorf(http.StatusInternalServerError, message)
	}

	log.Printf("enqueued task: id=%s queue=%s", info.ID, info.Queue)

	return cart.CheckoutResponse{OrderNumber: ""}, nil
}

func main() {
	host := "host.k3d.internal"
	redisAddr := fmt.Sprintf("%s:6379", host)
	//host := "localhost"

	asynqClient := asynq.NewClient(asynq.RedisClientOpt{Addr: redisAddr})
	defer asynqClient.Close()

	// open db connection
	dbModule, err := database.New("mysql", fmt.Sprintf("root:root@tcp(%s:3306)/commerce?parseTime=true", host))
	if err != nil {
		log.Fatalln("unable to connect to database", err)
	}

	cartRepo := &cart.CartRepoMySQL{DB: dbModule}

	productClient := product.RESTService{
		BaseURL: "http://be-product.default.svc.cluster.local",
		//BaseURL: "http://localhost:8080",
		Source: "cart",
	}

	gotenbergClient := gotenberg.RESTService{
		BaseURL: "http://gotenberg.default.svc.cluster.local",
		//BaseURL: "http://localhost:8080",
		Source: "cart",
	}

	service := &Service{asynqClient: asynqClient, cartRepo: cartRepo, product: productClient, gotenberg: gotenbergClient}
	http.ListenAndServe(":9000", service)
}
