import http from "k6/http";
import { uuidv4 } from "https://jslib.k6.io/k6-utils/1.4.0/index.js";
import { check, sleep } from "k6";

export default function () {
  const randomUUID = uuidv4();

  // create a new cart
  const cartRes = http.get("http://localhost:8080/cart/" + randomUUID);

  check(cartRes, { "cart created": (r) => r.status == 200 });

  const cartID = cartRes.json("id");

  // add product to the cart

  const payload = JSON.stringify({
    product_id: "product-001",
    quantity: 2,
  });

  const addItemRes = http.post(
    `http://localhost:8080/cart/${cartID}/add`,
    payload
  );

  check(addItemRes, { "new item added": (r) => r.status == 204 });

  // checkout
  const checkoutPayload = JSON.stringify({
    payment_method: "stripe",
    address: "Number street, City, Country 1190",
  });

  const res = http.post(
    `http://localhost:8080/cart/${cartID}/checkout`,
    checkoutPayload
  );

  check(res, { checkout: (r) => r.status == 200 });

  console.log(cartID);
  sleep(1);
}
