package gotenberg

import (
	"context"
	"fmt"
	"gitlab.com/wayanjimmy/commerce/pkg/rest"
)

type RESTService struct {
	BaseURL string
	Source  string
}

func (s *RESTService) GeneratePDF(ctx context.Context, targetUrl string) ([]byte, error) {
	url := fmt.Sprintf("%s/forms/chromium/convert/url", s.BaseURL)

	res, err := rest.
		RequestWithContext(ctx, s.Source).
		SetMultipartFormData(map[string]string{
			"url": targetUrl,
		}).
		Post(url)
	if err != nil {
		return nil, err
	}

	return res.Body(), err
}
